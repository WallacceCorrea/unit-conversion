# Unit Conversion
Exercise proposed by Scrimba course.
A  MVP Unit Conversion website made in JS. The Figma Mockup was posted by Scrimba's FrontEnd Carrer Path Course.
The layout was provided on Figma.

## Basic Requirements
- Save number to a variable in code
- When app loads, do calculations and display the results
- Round numbers to 3 decimals places
- Strech: Add an input for the users to change the number and automatically recalculate values when it changes.

## Added by myself
- Validation for integer number only
- The last conversion still appearing until a new operation
