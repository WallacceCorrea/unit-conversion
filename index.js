//getElements
const inputEl = document.getElementById("value-input");
const lengthEl = document.getElementById("length");
const volumeEl = document.getElementById("volume");
const massEl = document.getElementById("mass");

//setVariables
let meter = 0;
let feet = 0;
let liter = 0;
let gallon = 0;
let kilo = 0;
let pound = 0;

//Calls validation
inputEl.addEventListener("change", isNum());

//if the input IsValid runs the code.
function isNum() {
  let isNum = /^\d+$/.test(+inputEl.value);
  if (isNum) {
    let baseNumber = ""; //store the from input value for calculation
    baseNumber += inputEl.value;
    getNumber(baseNumber);
  } else {
    alert("Ops! Let's try an integer now?");
    inputEl.value = "0";
  }
}

//Main function - it casts the validated value to an integer and passes to the 'doCalc' function
function getNumber(baseNumber) {
  baseNumber = +inputEl.value;
  inputEl.value = "";
  doCalc(baseNumber);
}
//it does  conversions and call display functions
function doCalc(value) {
  meter2Feet(value);
  feet2Meter(value);
  liter2Gallon(value);
  gallon2Liter(value);
  kilo2Pound(value);
  pound2Kilo(value);
  displayResults(value);
  showLastValue(value);
}

// Calculations

function meter2Feet(value) {
  feet = round(value * 3.28084);
}

function feet2Meter(value) {
  meter = round(value * 0.3048);
}

function gallon2Liter(value) {
  gallon = round(value * 3.78541);
}

function liter2Gallon(value) {
  liter = round(value * 0.264172);
}

function kilo2Pound(value) {
  pound = round(value * 2.20462);
}

function pound2Kilo(value) {
  kilo = round(value * 0.453592);
}

// display

function displayResults(value) {
  lengthEl.textContent =
    value +
    " meters" +
    " = " +
    feet +
    " feets" +
    " | " +
    value +
    " feets" +
    " = " +
    meter +
    " meters";

  volumeEl.textContent =
    value +
    " liters" +
    " = " +
    gallon +
    " gallons" +
    " | " +
    value +
    " gallons" +
    " = " +
    liter +
    " liters";
  massEl.textContent =
    value +
    " kilos" +
    " = " +
    pound +
    " pounds" +
    " | " +
    value +
    " pounds" +
    " = " +
    kilo +
    " kilos";
}

//utility

function round(value) {
  return value.toFixed(3);
}

function showLastValue(value) {
  inputEl.placeholder = value;
}
